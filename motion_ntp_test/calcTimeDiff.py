#!/usr/bin/python
import numpy as np
import sys
import os
import time
import pdb
from datetime import datetime,timezone

#pdb.set_trace()
	
def main(argv):
	input_file = ''
	output_file = ''
	fields = []
	time_stamps = [] 
	time_stamps1 = [] 
	time_stamps2 = [] 
	time_diffs = []
	times_sec_ns = []
	time_stamp = int
	check_duplicate_value = 0
	epoch_sec = 0
	dateepoch = []
	hour_min_sec = []
	pvname = ''
	pvname1 = ''
	pvname2 = ''
	ref_val = 0
#Open file in read only "r" mode
	with open(sys.argv[1], "r") as input_file:
		for idxno, line in enumerate(input_file):
			fields = line.split()
			#find PV name
			pvname = fields[0]
			if pvname1 ==  '':
				pvname1 = pvname
			if pvname1 != pvname and pvname2 == '':
				pvname2 = pvname
#			print("name{}".format(pvname))	
#			print("line: {}".format(idxno))

			#check if array or not
			if fields[-1].find(".") > -1:
				is_float = 1
#				print(": {}".format(fields[-1]))
			else:
				
				is_float = -1
			
			try:
				is_integer = int(fields[-1])
			except:
				is_integer = -1
#			print("pv: {} {} \n".format(pvname, pvname1));	
			#check for PV name
			if pvname1 == pvname:
				# convert to epoch
				year_month_day = fields[-2].split('-')
#				print("ymd {}".format(year_month_day))
				hour_min_sec = fields[-1].split(':')
#				print("hms {}".format(hour_min_sec))
				sec_msec = hour_min_sec[-1].split('.')
#				print("ms {}".format(sec_msec))
                #time in seconds without decimals
				epoch = datetime(1970,1,1,tzinfo=timezone.utc)
				log_timedate = datetime(int(year_month_day[0]),int(year_month_day[1]),int(year_month_day[2]),int(hour_min_sec[0]),int(hour_min_sec[1]),int(sec_msec[0]),tzinfo=timezone.utc)
				epoch_sec = int(log_timedate.timestamp()*1000000000 + int(sec_msec[1])*1000000)
#				print("pv1 {:d}".format(epoch_sec))
#				time_stamp = epoch_sec * 1000000000
				time_stamps1.append(int(epoch_sec/1000000))

				
			if pvname2 == pvname:

				times_sec_ns = fields[4:]
				for fieldidx, timevalue in enumerate(times_sec_ns):
					if int(timevalue) > 1000000000:
						time_stamps2.append(int((int(times_sec_ns[fieldidx]) * 1000000000 + int(times_sec_ns[fieldidx+1]))/1000000))
#						print("pv2 {}".format((int(times_sec_ns[fieldidx]) * 1000000000 + int(times_sec_ns[fieldidx+1]))))
					
	no_of_elements = int(len(time_stamps)-1)
	folderfilename = sys.argv[1].split("/")
	filename = folderfilename[-1]
	mini = 99999999999999999999999
	maxi = 0
	idx_no = 0
	if not os.path.exists("output"):
		os.makedirs("output")
	with open("output/"+filename+"_output","w") as output_file:
		output_file.write("npt in nanoseconds | TS in (ns) |  diff (ns)\n")
		for x, time in enumerate(time_stamps1[:no_of_elements-2]):
			idx_no = idx_no + 1
			if ref_val == 0:
				ref_val = time_stamps1[x] - time_stamps2[x]
			if time_stamps1[x] - time_stamps2[x] - ref_val > maxi:
				maxi = int(time_stamps1[x] - time_stamps2[x] - ref_val)
			if time_stamps1[x] - time_stamps2[x] - ref_val < mini:
				mini = int(time_stamps1[x] - time_stamps2[x] - ref_val)
			print("{} {} | {}    | {}".format(idx_no*2, time_stamps1[x], time_stamps2[x], time_stamps1[x] - time_stamps2[x] - ref_val))
			output_file.write(" {} | {}    | {}\n".format(time_stamps1[x], time_stamps2[x], time_stamps1[x] - time_stamps2[x] - ref_val))
		output_file.write("Min: {} \nMax: {}\n".format(mini, maxi))
		print("Min: {:d} \nMax: {:d}\n".format(mini, maxi))
		#pdb.set_trace()
		
if __name__ == "__main__":
	main(sys.argv[1:])
