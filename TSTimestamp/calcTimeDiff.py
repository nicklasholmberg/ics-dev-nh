#!/usr/bin/python
import numpy as np
import sys
import os
import time
import pdb

#pdb.set_trace()
    
def main(argv):
    input_file = ''
    output_file = ''
    fields = []
    time_stamps = [] 
    time_diffs = []
    times_sec_ns = []
    time_stamp = int
    check_duplicate_value = 0
    pvname = ''
    absTimeStamp = 0
    periodDiff = 0
    periodTime  = 0
    maxi = 0
    mini = 0
#Open file in read only "r" mode
    with open(sys.argv[1], "r") as input_file:
        for idxno, line in enumerate(input_file):
            if idxno > 0:
                fields = line.split()
                #find PV name
                if pvname ==  '':
                    pvname = fields[0]
                #print("name{}".format(pvname)) 
                #print("line: {}".format(idxno))

                #check if array or not
                if fields[-1].find(".") > -1 and len(fields) > 3:
                    is_float = 1
                    #print(": {}".format(fields[-1]))
                else:
                    
                    is_float = -1
                
                try:
                    is_integer = int(fields[-1])
                except:
                    is_integer = -1
                
                #check for PV name
                if pvname == fields[0] :
                    # Float or array
                    if is_float > -1:
                        
                        intdec = fields[-1].split(".")
                        time_stamp = int(intdec[0])*1000000000+int(intdec[1])
                        time_stamps.append(time_stamp)
                    
                    if is_integer > -1: 
                        times_sec_ns = fields[4:]
                        for fieldidx, timevalue in enumerate(times_sec_ns):
                            if int(timevalue) > 1000000000:
                                time_stamps.append(int(times_sec_ns[fieldidx]) * 1000000000 + int(times_sec_ns[fieldidx+1]))
                    
    no_of_elements = int(len(time_stamps)-1)
    folderfilename = sys.argv[1].split("/")
    filename = folderfilename[-1]
    mini = 0
    maxi = 0
    if not os.path.exists("output"):
        os.makedirs("output")
    with open("output/"+filename+"_output","w") as output_file:
        output_file.write("epoch in nanoseconds | period (ns) | period diff (ns)\n")
        for x, time in enumerate(time_stamps[:no_of_elements-3]):
            absTimeStamp = time_stamps[x+1]
            periodTime = time_stamps[x+1] - time_stamps[x]
            periodDiff = (time_stamps[x+2] - time_stamps[x+1]) - (time_stamps[x+1] - time_stamps[x])
            print("{} | {} | {}    | {}".format(x, absTimeStamp, periodTime, periodDiff))
            #print(" {} | {}    | {}".format(time_stamps[x+1], (time_stamps[x+1] - time_stamps[x]), ((time_stamps[x+2] - time_stamps[x+1]) - (time_stamps[x+1] - time_stamps[x]))))
            if periodDiff > maxi:
                print("Maxi {}".format(periodDiff))
                maxi = periodDiff
            if periodDiff  < mini:
                print("Mini {}".format(periodDiff))
                mini = periodDiff
            #if ((time_stamps[x+2] - time_stamps[x+1]) - (time_stamps[x+1] - time_stamps[x])) > maxi:
            #   maxi = ((time_stamps[x+2] - time_stamps[x+1]) - (time_stamps[x+1] - time_stamps[x]))
            #if ((time_stamps[x+2] - time_stamps[x+1]) - (time_stamps[x+1] - time_stamps[x])) < mini:
            #   mini = ((time_stamps[x+2] - time_stamps[x+1]) - (time_stamps[x+1] - time_stamps[x]))
            output_file.write("{}  | {} | {}    | {}\n".format(x,absTimeStamp, periodTime, periodDiff))
        output_file.write("Min: {} \nMax: {}\n".format(mini, maxi))
        print("Min: {} \nMax: {}\n".format(mini, maxi))
        #pdb.set_trace()
        
if __name__ == "__main__":
    main(sys.argv[1:])
