//<input_file> is created by camonitor:

//camonitor LabS-Utgard-VIP:TS-EVR-1:Timestamp_stringin > input/"filename"
//camonitor LabS-Utgard-VIP:Chop-Drv-0202:TDC_array > input/"filename"
//camonitor LabS-Utgard-VIP:Chop-Drv-0202:TDC_array 2>&1 | tee input/"filename"


//<input_file> can have the one of two data formats:
//LabS-Utgard-VIP:TS-EVR-1:Timestamp_stringin 2018-10-24 09:25:24.364211 1540373124.364210908
//LabS-Utgard-VIP:Chop-Drv-0102:TDC_array 2019-03-25 10:25:14.280605 6 1553505914 227034178 1553505914 244891320 1553505914 262748462
 

//script is called by:
//$python calcTimeDiff.py <input_file>

//output folder is created if it does not exist
//output file in named  <input_file>_output
