#!/usr/bin/python
import numpy as np
import sys
import os
import time
import pdb
import matplotlib.pyplot as plt
from datetime import datetime
#pdb.set_trace()
    
def main(argv):
    input_file = ''
    output_file = ''
    fields = []
    chrony_time_stamps = [] 
    chrony_offset = []
    chrony_timestamp = []
    chrony_time_min_offset = 0
    chrony_time_max_offset = 0.0
    chrony_start_time = 0.0
    chrony_stop_time = 0.0
    chrony_total_time = 0.0
    chrony_min = 0
    chrony_max = 0
    chrony_max_delay = 0.0
    chrony_min_delay = 99999999999.0
    chrony_offset_trunc = []
    chrony_timestamp_trunc = []
    chrony_timestamp_len = 0
    phc2sys_time_stamps = [] 
    phc2sys_offset = []
    phc2sys_timestamp = []
    phc2sys_time_min_offset = 0
    phc2sys_time_max_offset = 0.0
    phc2sys_start_time = 0.0
    phc2sys_first_time = 0.0
    phc2sys_stop_time = 0.0
    phc2sys_total_time = 0.0
    phc2sys_min = 0
    phc2sys_max = 0
    phc2sys_max_delay = 0.0
    phc2sys_min_delay = 99999999999.0
# Draw chrony statistics
# Open file in read only "r" mode
    with open(sys.argv[1], "r") as input_file:
        for idxno, line in enumerate(input_file):
            fields = line.split()
            offset_is_int = 0
            try:
                float(fields[4])
                offset_is_int = 1
#                print("value {}".format(fields[4]))
            except:
                offset_is_int = -1
            if offset_is_int == 1 and idxno > 15:
                chrony_offset.append(1E9*float(fields[4]))
                if chrony_max < 1E9*float(fields[4]):
                    chrony_max = 1E9*float(fields[4])
                    chrony_time_max_offset = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                if chrony_min > 1E9*float(fields[4]):
                    chrony_min = 1E9*float(fields[4])
                    chrony_time_min_offset = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                if chrony_start_time == 0:
                    chrony_start_time = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                chrony_stop_time = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                chrony_timestamp.append(int((datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S') - chrony_start_time).total_seconds()))

    chrony_total_time = (chrony_stop_time - chrony_start_time).total_seconds()

    
          
    print("chrony_min_offset:  {}, chrony_max_offset:       {}".format(chrony_min,chrony_max))
    print("chrony_start_time:       {}".format( chrony_start_time))
    print("chrony_timestamp_len:  {}, chrony_start_time:       {}".format(len(chrony_timestamp), chrony_start_time))
# Draw chrony statistics
    plt.plot(chrony_timestamp,chrony_offset)
# Draw phc2sys statistics
    plt.draw()
    plt.show()
if __name__ == "__main__":
    main(sys.argv[1:])
