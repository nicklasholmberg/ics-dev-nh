#!/usr/bin/python
import numpy as np
import sys
import os
import time
import pdb
import matplotlib.pyplot as plt
from datetime import datetime
#pdb.set_trace()
    
def main(argv):
    input_file = ''
    output_file = ''
    fields = []
    chrony_time_stamps = [] 
    chrony_offset = []
    chrony_timestamp = []
    chrony_time_min_offset = 0
    chrony_time_max_offset = 0.0
    chrony_start_time = 0.0
    chrony_stop_time = 0.0
    chrony_total_time = 0.0
    chrony_min = 0
    chrony_max = 0
    chrony_max_delay = 0.0
    chrony_min_delay = 99999999999.0
    chrony_offset_trunc = []
    chrony_timestamp_trunc = []
    chrony_timestamp_len = 0
    phc2sys_time_stamps = [] 
    phc2sys_offset = []
    phc2sys_timestamp = []
    phc2sys_time_min_offset = 0
    phc2sys_time_max_offset = 0.0
    phc2sys_start_time = 0.0
    phc2sys_first_time = 0.0
    phc2sys_stop_time = 0.0
    phc2sys_total_time = 0.0
    phc2sys_min = 0
    phc2sys_max = 0
    phc2sys_max_delay = 0.0
    phc2sys_min_delay = 99999999999.0
# Draw chrony statistics
# Open file in read only "r" mode
    with open(sys.argv[1], "r") as input_file:
        for idxno, line in enumerate(input_file):
            fields = line.split()
            offset_is_int = 0
            try:
                float(fields[4])
                offset_is_int = 1
#                print("value {}".format(fields[4]))
            except:
                offset_is_int = -1
            if offset_is_int == 1 and idxno > 15:
                chrony_offset.append(1E9*float(fields[4]))
                if chrony_max < 1E9*float(fields[4]):
                    chrony_max = 1E9*float(fields[4])
                    chrony_time_max_offset = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                if chrony_min > 1E9*float(fields[4]):
                    chrony_min = 1E9*float(fields[4])
                    chrony_time_min_offset = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                if chrony_start_time == 0:
                    chrony_start_time = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                chrony_stop_time = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                chrony_timestamp.append(int((datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S') - chrony_start_time).total_seconds()))

    with open(sys.argv[2], "r") as input_file:
        for idxno, line in enumerate(input_file):
            fields = line.split()
            offset_is_int = 0
            delay_is_int = 0
            if idxno == 0:
                try:
                    phc2sys_start_time = datetime.strptime(fields[0]+ ' ' + fields[1], '%Y-%m-%d %H:%M:%S')
                except:
                    phc2sys_start_time = 0
            try:
                int(fields[4])
                delay_is_int = 1
            except:
                delay_is_int = -1
            try:
                int(fields[9])
                offset_is_int = 1
            except:
                offset_is_int = -1
            #print("value {}".format(fields))
            if delay_is_int == 1 and offset_is_int == 1 and idxno > 15:
                phc2sys_offset.append(int(fields[4]))
                if phc2sys_first_time == 0:
                    phc2sys_first_time = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                if phc2sys_max < int(fields[4]):
                    phc2sys_max = int(fields[4])
                    phc2sys_time_max_offset = int(float(fields[0].split('[', 1)[1].split(']')[0]))-phc2sys_first_time
                if phc2sys_min > int(fields[4]):
                    phc2sys_min = int(fields[4])
                    phc2sys_time_min_offset = int(float(fields[0].split('[', 1)[1].split(']')[0]))-phc2sys_first_time
                if phc2sys_max_delay < int(fields[9]):
                    phc2sys_max_delay = int(fields[9])
                    phc2sys_time_max_delay = int(float(fields[0].split('[', 1)[1].split(']')[0]))-phc2sys_first_time
                if phc2sys_min_delay > int(fields[9]):
                    phc2sys_min_delay = int(fields[9])
                    phc2sys_time_min_delay = int(float(fields[0].split('[', 1)[1].split(']')[0]))-phc2sys_first_time
                phc2sys_stop_time = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                phc2sys_total_time = phc2sys_stop_time - phc2sys_first_time
                phc2sys_timestamp.append(int(float((fields[0].split('[', 1)[1].split(']')[0])))- phc2sys_first_time)

    chrony_total_time = (chrony_stop_time - chrony_start_time).total_seconds()
    if chrony_total_time > phc2sys_total_time:
        chrony_timestamp_trunc = list(filter(lambda x: x > (int(chrony_total_time)-int(phc2sys_total_time)), chrony_timestamp))
        chrony_corr_timestamp_trunc = list(x-(int(chrony_total_time)-int(phc2sys_total_time)) for x in chrony_timestamp_trunc)
        chrony_timestamp_len = len(chrony_timestamp_trunc)
#        print("chrony_ts_trunc_start: {}".format(chrony_corr_timestamp_trunc[0]))
#        print("chrony_ts_trunc: {}".format(chrony_timestamp_len))
#        print("phc2sys_ts_trunc: {}".format(len(phc2sys_offset)))
        chrony_offset_trunc = chrony_offset[-chrony_timestamp_len:]
#    print("phc2sys_offset: {}".format(phc2sys_offset))
    
          
    print("phc2sys_total_time: {}, chrony_total_time:       {}".format(phc2sys_total_time, chrony_corr_timestamp_trunc[-1]))
    print("phc2sys_min_offset: {}, phc2sys_time_min_offset: {}".format(phc2sys_min, phc2sys_time_min_offset))
    print("phc2sys_max_offset: {}, phc2sys_time_max_offset: {}".format(phc2sys_max, phc2sys_time_max_offset))
    print("chrony_min_offset:  {}, chrony_max_offset:       {}".format(min(chrony_offset_trunc),max(chrony_offset_trunc)))
    print("phc2sys_start_time:  {}, chrony_start_time:       {}".format(phc2sys_start_time, chrony_start_time))
    print("chrony_timestamp_len:  {}, chrony_start_time:       {}".format(len(chrony_timestamp), chrony_start_time))
# Draw chrony statistics
    plt.plot(chrony_corr_timestamp_trunc,chrony_offset_trunc, color='blue', label ="chrony statistics")
# Draw phc2sys statistics
    plt.plot(phc2sys_offset, color='green', label="phc2sys offset")
    plt.legend()
    plt.draw()
    plt.show()
if __name__ == "__main__":
    main(sys.argv[1:])
