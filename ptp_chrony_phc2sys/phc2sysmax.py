#!/usr/bin/python
import numpy as np
import sys
import os
import time
import pdb
import matplotlib.pyplot as plt
#pdb.set_trace()
    
def main(argv):
    input_file = ''
    output_file = ''
    fields = []
    time_stamps = [] 
    time_diffs = []
    times_sec_ns = []
    time_stamp = int
    check_duplicate_value = 0
    time_min_offset = 0.0
    time_max_offset = 0.0
    time_max_delay = 0.0
    time_min_delay = 0.0
    start_time = 0.0
    stop_time = 0.0
    total_time = 0.0
    min = 0
    max = 0
    max_delay = 0.0
    min_delay = 99999999999.0
#Open file in read only "r" mode
    with open(sys.argv[1], "r") as input_file:
        for idxno, line in enumerate(input_file):
            fields = line.split()
            offset_is_int = 0
            delay_is_int = 0
            try:
                int(fields[4])
                delay_is_int = 1
            except:
                delay_is_int = -1
            try:
                int(fields[9])
                offset_is_int = 1
            except:
                offset_is_int = -1
            #print("value {}".format(fields))
            if delay_is_int == 1 and offset_is_int == 1 and idxno > 15:
                time_stamps.append(int(fields[4]))
                if max < int(fields[4]):
                    max = int(fields[4])
                    time_max_offset = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                if min > int(fields[4]):
                    min = int(fields[4])
                    time_min_offset = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                if max_delay < int(fields[9]):
                    max_delay = int(fields[9])
                    time_max_delay = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                if min_delay > int(fields[9]):
                    min_delay = int(fields[9])
                    time_min_delay = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                if start_time == 0:
                    start_time = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                stop_time = int(float(fields[0].split('[', 1)[1].split(']')[0]))
                total_time = stop_time - start_time
        plt.figure(1)
        plt.plot(time_stamps)
        plt.draw()
        print("total_time: {}".format(total_time))
        print("min_offset: {}, time_min_offset: {}".format(min, time_min_offset-start_time))
        print("max_offset: {}, time_max_offset: {}".format(max, time_max_offset-start_time))
        print("max_delay:  {}, time_max_delay:  {}".format(max_delay, time_max_delay-start_time))
        print("min_delay:  {}, time_min_delay:  {}".format(min_delay, time_min_delay-start_time))
        plt.figure(2)
        plt.hist(time_stamps,bins=20)
        plt.show()
if __name__ == "__main__":
    main(sys.argv[1:])
