from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()

# link to definition file, local or in repo, to sub device
#factory.addLink("detector_utils", "EPI[beckhoff]", "https://bitbucket.org/europeanspallationsource/cms-string", "/home/nicklasholmberg2/git/ics-dev-nh/ics_plc_factory/beckhoff.def")

# Name to specify as argument in plcfactory.py
plc = factory.addBECKHOFF("Utg-VIP:Det-Util-01")

# link to definition file, local or in repo, to PLC device
plc.addLink("EPI[beckhoff]", "https://bitbucket.org/europeanspallationsource/cms-string", "/home/nicklasholmberg2/git/ics-dev-nh/ics_plc_factory/beckhoff.def")

# two devices controlled by the PLC, if using factory.addLink
cmses = [ "Utg-VIP:Det-Util-001" "Utg-VIP:Det-Util-002"]

# add sub devices to PLC if needed
#plc.setControls(cmses)

#Go through all the devices in cmses
for cms in cmses:
    factory.addDevice("detector_utils", cms)

factory.dump("beckhoff")
