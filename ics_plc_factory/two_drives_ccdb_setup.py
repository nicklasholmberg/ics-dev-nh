from ccdb_factory import CCDB_Factory

factory = CCDB_Factory()

# link to definition file, local or in repo, to sub device
#factory.addLink("detector_utils", "EPI[beckhoff]", "https://bitbucket.org/europeanspallationsource/cms-string", "/home/nicklasholmberg2/git/ics-dev-nh/ics_plc_factory/beckhoff.def")

# Add the PLC device with name
plc = factory.addBECKHOFF("chic_01")

# link to definition file, local or in repo, to PLC device
factory.addLink("chic_type", "EPI[CHOP_CHIC.def]", "https://bitbucket.org/europeanspallationsource/chop_chic")

# two devices controlled by the PLC, if using factory.addLink
drives = ["drv1", "drv2"]

# add sub devices to PLC if needed
plc.setControls(drives)

#Go through all the devices in cmses
for drv in drives:
    factory.addDevice("chic_type", drv)

factory.device("drv1").addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")
factory.device("drv2").addLink("EPI[CHOP_DRV.def]", "https://bitbucket.org/europeanspallationsource/chop_drv")

# Dump our CCDB
factory.dump("ccdbdump")
